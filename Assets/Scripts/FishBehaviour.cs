﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FishBehaviour : MonoBehaviour {

	// Use this for initialization
	void Start () {
        FlipTail();

	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.F))
        {
            FlipFish();
        }
	}

    public void FlipFish()
    {
        if (transform.GetComponent<SpriteRenderer>().flipX)
        {
            transform.GetComponent<SpriteRenderer>().flipX = false;

        }
        else
        {
            transform.GetComponent<SpriteRenderer>().flipX = true;
        }
    }

    void FlipTail()
    {
        float speed = Random.Range(1.0f, 4f);
        speed = 1 / speed;
        int yExtent = Random.Range(25, 35);
        int zExtent = Random.Range(2, 5);

        // Rotate along y-axis 
        Sequence fishSequence = DOTween.Sequence();
        fishSequence.Append(transform.DORotate(new Vector3(transform.rotation.x, -yExtent, -zExtent), speed));
        fishSequence.Append(transform.DORotate(new Vector3(transform.rotation.x, 0, 0), speed));
        fishSequence.Append(transform.DORotate(new Vector3(transform.rotation.x, yExtent, zExtent), speed));
        fishSequence.Append(transform.DORotate(new Vector3(transform.rotation.x, 0, 0), speed));
        fishSequence.SetLoops(-1, LoopType.Restart);

    }

    void UpDown()
    {
        float speed = Random.Range(0.5f, 1.2f);
        speed = 1 / speed;
        // Rotate along z-axis
        Sequence fishSequence = DOTween.Sequence();

        fishSequence.Append(transform.DORotate(new Vector3(transform.rotation.x, transform.rotation.y, 0), speed));
        fishSequence.Append(transform.DORotate(new Vector3(transform.rotation.x, transform.rotation.y, 5), speed));
        fishSequence.Append(transform.DORotate(new Vector3(transform.rotation.x, transform.rotation.y, 0), speed));
        fishSequence.SetLoops(-1, LoopType.Restart);
    }
}
