﻿using UnityEngine;
using System.Collections;

public class VelocityFieldFish : MonoBehaviour {
	public float visc = 0.01f;
	public int iterations = 10;
	public float particleSpeed = 100;
	/*public Texture2D border;
	public Texture2D flow;*/
	
	Texture2D tex;
	int width, height;
	float[,] u, v, u_prev, v_prev;
	int numParticles = 1024;
    public Particle[] particles;

    private float px, py;
	/*float[,] bndX, bndY;
	float[,] velX, velY;*/

    public static VelocityFieldFish instance;

    private float lastTouch = 0;

    private int pertX, pertY;
    private float pertU, pertV;
    private bool pertubating = false;

    void Awake()
    {
        instance = this;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

	void Start()
	{
		// duplicate the original texture and assign to the material
		tex = Instantiate(GetComponent<Renderer>().material.mainTexture) as Texture2D;
		GetComponent<Renderer>().material.mainTexture = tex;
		// get grid dimensions from texture
		width = tex.width;
		height = tex.height;
		// initialize velocity arrays
		u = new float[width, height];
		v = new float[width, height];
		u_prev = new float[width, height];
		v_prev = new float[width, height];
		// random position for particles
		particles = new Particle[numParticles];
		for (int i = 0; i < numParticles; i++) {
			particles[i].x = Random.Range(1, width-2);
			particles[i].y = Random.Range(1, height-2);
		    particles[i].isMovingThing = false;
		}
		/*// read border and velocity data from textures
		bndX = new float[width, height];
		bndY = new float[width, height];
		velX = new float[width, height];
		velY = new float[width, height];
		for (int j = 0; j < height-1; j++) {
			for (int i = 0; i < width-1; i++) {
				bndX[i, j] = border.GetPixel(i, j).grayscale - border.GetPixel(i+1, j).grayscale;
				bndY[i, j] = border.GetPixel(i, j).grayscale - border.GetPixel(i, j+1).grayscale;
				velX[i, j] = (flow.GetPixel(i, j).grayscale - flow.GetPixel(i+1, j).grayscale);
				velY[i, j] = (flow.GetPixel(i, j).grayscale - flow.GetPixel(i, j+1).grayscale);
			}
		}*/
	}
	
	void Update() {
		/*for (int j = 1; j < height-1; j++) {
			for (int i = 1; i < width-1; i++) {
				u[i, j] = velX[i, j];
				v[i, j] = velY[i, j];
			}
		}*/
		UserInput();
		VelStep();
		MoveParticles();
		Draw();
	}

	void SetBounds(int b, float[,] x) {
		/*// b/w texture as obstacles
		for (int j = 1; j < height; j++) {
			for (int i = 1; i < width; i++) {
				if (bndX[i, j] < 0) {
					x[i, j] = (b == 1) ? -x[i+1, j] : x[i+1, j];
				}
				if (bndX[i, j] > 0) {
					x[i, j] = (b == 1) ? -x[i-1, j] : x[i-1, j];
				}
				if (bndY[i, j] < 0) {
					x[i, j] = (b == 2) ? -x[i, j+1] : x[i, j+1];
				}
				if (bndY[i, j] > 0) {
					x[i, j] = (b == 2) ? -x[i, j-1] : x[i, j-1];
				}
			}
		}*/
		// rect borders
		float sign;
		// left/right: reflect if b is 1, else keep value before edge
		sign = (b == 1) ? -1 : 1;
		for (int i = 1; i < height-1; i++) {
			x[0, i] = sign * x[1, i];
			x[width-1, i] = sign * x[width-2, i];
		}
		// bottom/top: reflect if b is 2, else keep value before edge
		sign = (b == 2) ? -1 : 1;
		for (int i = 1; i < width-1; i++) {
			x[i, 0] = sign * x[i, 1];
			x[i, height-1] = sign * x[i, height-2];
		}
		// vertices
		x[0, 0]				 = 0.5f * (x[1, 0] + x[0, 1]);
		x[width-1, 0]		 = 0.5f * (x[width-2, 0] + x[width-1, 1]);
		x[0, height-1]		 = 0.5f * (x[1, height-1] + x[0, height-2]);
		x[width-1, height-1] = 0.5f * (x[width-2, height-1] + x[width-1, height-2]);
	}

	void Diffuse(int b, float[,] x, float[,] x0, float a, float c) {
		for (int k = 0; k < iterations; k++) {
			for (int j = 1; j < height-1; j++) {
				for (int i = 1; i < width-1; i++) {
					x[i, j] = (x0[i, j] + a * (x[i-1, j] + x[i+1, j] + x[i, j-1] + x[i, j+1])) / c;
				}
			}
			SetBounds(b, x);
		}
	}
	
	void Project(float[,] u, float[,] v, float[,] p, float[,] div) {
		float h = 1 / Mathf.Sqrt(width * height);
		for (int j = 1; j < height-1; j++ ) {
			for (int i = 1; i < width-1; i++) {
				div[i, j] = -0.5f * h * (u[i+1, j] - u[i-1, j] + v[i, j+1] - v[i, j-1]);
				p[i, j] = 0;
			}
		}
		SetBounds(0, div);
		SetBounds(0, p);
		
		Diffuse(0, p, div, 1, 4);

		for (int j = 1; j < height-1; j++) {
			for (int i = 1; i < width-1; i++) {
				u[i, j] -= 0.5f * (p[i+1, j] - p[i-1, j]) / h;
				v[i, j] -= 0.5f * (p[i, j+1] - p[i, j-1]) / h;
			}
		}
		SetBounds(1, u);
		SetBounds(2, v);
	}

	void VelStep() {
		float a = Time.deltaTime * visc * width * height;
		Diffuse(1, u, u, a, 1 + 4 * a);
		Diffuse(2, v, v, a, 1 + 4 * a);
		Project(u, v, u_prev, v_prev);
	}
	
	void UserInput() {

	    if (Input.touchCount > 0)
	    {
	        lastTouch = 0;
	        foreach (Touch t in Input.touches)
	        {
	            Ray ray = Camera.main.ScreenPointToRay(t.position);
	            RaycastHit hit;
	            if (Physics.Raycast(ray, out hit, 100))
	            {
	                // determine indices where the user clicked
	                int x = (int) (hit.point.x * width);
	                int y = (int) ((hit.point.z + 0.33f) * width);
	                if (x < 1 || x >= width || y < 1 || y >= height) return;
	                // add velocity

	                //Debug.Log("Drag " + pointer_x + " " + pointer_y);
	                u[x, y] += t.deltaPosition.x / 200;
	                v[x, y] += t.deltaPosition.y / 200;

	                if (t.tapCount > 0)
	                {
	                    Debug.Log("Taps: " + t.tapCount);
	                }

	            }
	        }
	    }
        else if (Input.GetMouseButton(0)) {
	        lastTouch = 0;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, 100)) {
				// determine indices where the user clicked
				int x = (int)(hit.point.x * width);
                int y = (int)((hit.point.z + 0.28f) * width / 0.9f);
				if (x < 1 || x >= width || y < 1 || y >= height) return;
				// add velocity
			    float pointer_x = Input.GetAxis("Mouse X") / 10;
			    float pointer_y = Input.GetAxis("Mouse Y") / 10;
			    if (Input.touchCount > 0)
			    {
			        pointer_x = Input.touches[0].deltaPosition.x / 100;
			        pointer_y = Input.touches[0].deltaPosition.y / 100;
			    }


                //Debug.Log("Pos: " + x + " " + y);
                //Debug.Log("Drag " + pointer_x + " " + pointer_y); 2e3192 / 46 49 146
                u[x, y] += pointer_x;
                v[x, y] += pointer_y;
			}
		}
	    lastTouch += Time.deltaTime;

	    if (lastTouch > 0.5f)
	        visc = 0.00005f;
	    else
	    {
	        visc = 0.00015f;
	    }

	    if (lastTouch > 1)
	    {
            float maxPerturbation = 0.00005f;
	        float pertDuration = 2f;
            
            if (!pertubating)
	        {
	            pertX = Random.Range(0, width - 20);
	            pertY = Random.Range(0, height - 20);
                pertU = Random.Range(-maxPerturbation, maxPerturbation);
                pertV = Random.Range(-maxPerturbation, maxPerturbation);
	            pertubating = true;
	        }

	        if (lastTouch > 1 + pertDuration)
	        {
	            pertubating = false;
	            lastTouch = Random.Range(-3.0f, -1.0f);
	        }

	        if (pertubating)
	        {
	            for (int x1 = 0; x1 < 20; x1++)
	            {
	                for (int y1 = 0; y1 < 20; y1++)
	                {
	                    u[pertX + x1, pertY + y1] += pertU;
                        v[pertX + x1, pertY + y1] += pertV;
	                }
	            }
	        }
	    }

	}
	
	void Draw() {
		// visualize water
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
                //tex.SetPixel(x, y, new Color(u[x, y] * 30 + 0.5f, v[x, y] * 30 + 0.5f, 1)); 
                tex.SetPixel(x, y, new Color(u[x, y] * 30 + 18.0f / 255.0f, v[x, y] * 30 + 31.0f / 255.0f, 61.0f / 255.0f)); //2e3192 / 46 49 146 // 18, 31, 61
			}
		}
		// visualize particles
        //bool drawnParticle = false;
		/*foreach (Particle p in particles) {
			tex.SetPixel((int)p.x, (int)p.y, new Color(1, 1, 1));
            //if (!drawnParticle)
            //{
            //    //Debug.DrawLine(new Vector3(p.x, p.y, 0), new Vector3(px, py, 0), Color.red, 20);
            //    drawnParticle = true;
            //    px = p.x;
            //    py = p.y;
            //    //Debug.Log("Particle: " + p.x + " " + p.y);
            //}
		}*/
		tex.Apply(false);
	}

	void MoveParticles() {
		for (int i = 0; i < numParticles; i++) {
			float pX = particles[i].x;
            float pY = particles[i].y;
            particles[i].timer -= Time.deltaTime;
		    particles[i].age += Time.deltaTime;
			particles[i].x = pX += Bilin(u, pX, pY) * particleSpeed;
			particles[i].y = pY += Bilin(v, pX, pY) * particleSpeed;
            if (pX < 1 || pX > width - 2 || pY < 1 || pY > height - 2 || particles[i].timer < 0)
            {

                if (particles[i].x < 1) particles[i].x = 1;
                if (particles[i].y < 1) particles[i].y = 1;
                if (particles[i].x > width - 2) particles[i].x = width - 2;
                if (particles[i].y > height - 2) particles[i].y = height - 2;

                if (particles[i].isMovingThing)
                {
                    // managed externally
                }
                else if (particles[i].timer < 0)
                {
                    particles[i].x = Random.Range(1, width - 2);
                    particles[i].y = Random.Range(1, height - 2);
                    particles[i].timer = Random.Range(15.0f, 30.0f); // seconds
                    particles[i].age = 0;
                }
			}
            //if (particles[i].mover != null)
            //{
            //    particles[i].mover.SetPosition(particles[i].x, particles[i].y);
            //}
		}
	}

	float Bilin(float[,] field, float xPos, float yPos) {
		xPos -= 0.5f;
		yPos -= 0.5f;
		// casting to int is like floor
		int xInt = Mathf.Clamp((int)xPos, 1, width-2);
		int yInt = Mathf.Clamp((int)yPos, 1, height-2);
		// so diff is always positive
		float xDiff = xPos - xInt;
		float yDiff = yPos - yInt;
		// bilinear interpolation
		float cRow = (1 - xDiff) * field[xInt, yInt] + xDiff * field[xInt+1, yInt];
		float nRow = (1 - xDiff) * field[xInt, yInt+1] + xDiff * field[xInt+1, yInt+1];
		return (1 - yDiff) * cRow + yDiff * nRow;
	}

	public struct Particle {
		public float x;
		public float y;
	    public float timer;
	    public float age;
	    public bool isMovingThing;
	}
}
