﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Skaimmadas : MonoBehaviour {

    float originalZrot;
    public float upBob = -15.0f;
    public float swimDuration = 50f;

    public static Skaimmadas instance;

    public Transform jaw;

    public Transform startTransform, startTransform2, hiddenTransform, skaiAngryTarget;

    public Transform myMovingThing;

    private bool isPlayable = false;

	void Start ()
	{
	    instance = this;
        //ResetPos();
        //originalZrot = transform.rotation.z;
        //Debug.Log(originalZrot.ToString());
        //UpDown();
        //Swim(swimDuration);
        DOTween.defaultEaseType = Ease.InOutQuad;
	    isPlayable = false;
	}
	
    void ResetPos()
    {
        transform.position = new Vector2(-10, Random.Range(5, 15));
    }

	void Update () 
    {
        if (isPlayable)
        {
            if (!myMovingThing.gameObject.activeSelf && Vector3.Distance(this.transform.position, myMovingThing.transform.position) > 2)
            {
                this.transform.position = Vector3.Lerp(this.transform.position, myMovingThing.transform.position, 0.05f);
            }
            else
            {
                myMovingThing.gameObject.SetActive(true);
                this.transform.parent = myMovingThing.transform;
            }
        }
		
	}

    void Swim(float duration)
    {

        transform.DOMove(new Vector3(120, Random.Range(2f, 20f), 0), duration);

    }

    public void SetJaw(float angle)
    {
        //Debug.Log("Setting Jaw to " +angle);
        jaw.localEulerAngles = Vector3.Lerp(jaw.localEulerAngles, new Vector3(0, 0, 65.0f - angle), 0.5f);
        
    }

    void UpDown()
    {
        float speed = Random.Range(0.5f, 1.2f);
        speed = 1 / speed;
        // Rotate along z-axis
        Sequence fishSequence = DOTween.Sequence();

        fishSequence.Append(transform.DORotate(new Vector3(transform.rotation.x, transform.rotation.y, originalZrot), speed));
        fishSequence.Append(transform.DORotate(new Vector3(transform.rotation.x, transform.rotation.y, originalZrot + upBob), speed));
        fishSequence.Append(transform.DORotate(new Vector3(transform.rotation.x, transform.rotation.y, originalZrot), speed));
        fishSequence.SetLoops(-1, LoopType.Restart);
    }


    internal void StartScreen()
    {
        this.transform.position = startTransform.position;
        this.transform.rotation = startTransform.rotation;
        this.transform.localScale = startTransform.localScale;

        this.transform.DOMove(startTransform2.position, 2);

    }

    internal void StartShouting()
    {
        this.transform.position = startTransform.position;
        this.transform.rotation = startTransform.rotation;
        this.transform.localScale = startTransform.localScale;

        Sequence angrySeq = DOTween.Sequence();
        angrySeq.Append(this.transform.DOMove(startTransform2.position, 2));
        angrySeq.Append(this.transform.DOMove(skaiAngryTarget.position, 30));
        angrySeq.Play();
    }

    internal void MainGame()
    {
        this.transform.DOMove(hiddenTransform.position, 2);
        
    }

    internal void PlayMe()
    {
        isPlayable = true;
    }
}
