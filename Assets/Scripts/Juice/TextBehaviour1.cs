﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class TextBehaviour1 : MonoBehaviour {

    public Color thisColor;
    private bool textFlows = true;
    public float flowSpeed = 10.0f;
	// Use this for initialization
	void Start () {
        TextAppear();
        StartCoroutine(TextDisappear());
	}
	
	// Update is called once per frame
	void Update () {
		if(textFlows)
        {
            TextFlow();
        }
	}

    public void TextAppear()
    {

        transform.GetComponent<Text>().DOFade(1, 4f);
        transform.DOScale(0.4f, 4f);

        Sequence wordSequence2 = DOTween.Sequence();
        float originalY = transform.position.y;
        wordSequence2.Append(transform.DOMoveY(originalY - 6, 0.5f));
        wordSequence2.Append(transform.DOMoveY(originalY, 1f));
        wordSequence2.SetLoops(-1, LoopType.Restart);
        wordSequence2.Play();
    }

    public IEnumerator TextDisappear(float delay = 7.0f)
    {

        yield return new WaitForSeconds(delay);
        Destroy(this.gameObject, 3.2f);
        transform.DOScale(0.2f, 3f);
        transform.GetComponent<Text>().DOFade(0.2f, 3f);
        
    }

    public void TextFlow()
    {
        transform.Translate(Vector2.left * flowSpeed * Time.deltaTime);  
    }
}
