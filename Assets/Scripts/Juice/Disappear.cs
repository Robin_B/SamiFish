﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Disappear : MonoBehaviour {
    public float intensity = -0.5f;
    public float speed = 4;
	// Use this for initialization
	void Start () {

    }

    public void Do()
    {
        Destroy(this.gameObject, speed);
        transform.DOScale(0.001f, speed);
        //  transform.GetComponent<SpriteRenderer>().Do(transform.localScale.x + intensity, speed);

    }

    // Update is called once per frame
    void Update () {
		
	}
}
