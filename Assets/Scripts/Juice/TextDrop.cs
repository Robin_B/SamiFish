﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TextDrop : MonoBehaviour {

	// Use this for initialization
	void Start () {
        DropBounce();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void DropBounce()
    {
        // 
        float speed = Random.Range(1.0f, 4f);
        speed = 1 / speed;

        Sequence wordSequence = DOTween.Sequence();
        wordSequence.Append(transform.DOMoveY(720, 0.5f));
        wordSequence.Append(transform.DOMoveY(680, 0.5f));
        wordSequence.Append(transform.DOMoveY(720, 0.5f));
        wordSequence.Append(transform.DOMoveY(680, 0.5f));
        wordSequence.Play();

    }
}
