﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class TextBehaviour3 : MonoBehaviour {

    public Color thisColor;
	// Use this for initialization
	void Start () {
        TextAppear();
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void TextAppear()
    {

        transform.GetComponent<Text>().DOFade(1f,4f);
        Sequence wordSequence2 = DOTween.Sequence();
        float origScale = transform.localScale.x;
       // wordSequence2.Append(transform.DOScale(origScale/4, 0.01f));

        wordSequence2.Append(transform.DOScale(origScale*2.5f, 0.6f));
        wordSequence2.Append(transform.DOScale(origScale*1.8f, 0.3f));
        wordSequence2.Append(transform.DOScale(origScale*2f, 0.3f));
        wordSequence2.OnComplete(Seq2);
        wordSequence2.Play();
    }

    void Seq2()
    {
        Sequence wordSequence2 = DOTween.Sequence();
        float originalY = transform.position.y;
        wordSequence2.Append(transform.DOMoveY(originalY - 0.6f, 1f));
        wordSequence2.Append(transform.DOMoveY(originalY, 1f));
        wordSequence2.SetLoops(-1, LoopType.Restart);
        wordSequence2.Play();
    }

    public IEnumerator TextDisappear(float delay = 7.0f)
    {

        yield return new WaitForSeconds(delay);
        Destroy(this.gameObject, 120f);
        transform.DOScale(0.1f, 120f);
        transform.GetComponent<Text>().DOFade(0.2f, 120f);
        
    }

}
