﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
public class TextGenerator : MonoBehaviour {

    // Create letters individually and plunk them into an array
    List<Transform> newTexts;
    public Transform textTemplate;
    public Transform textTemplate3d;
    public Transform targetText;
    public Transform levelText;
    public bool text3d = false;
    public Color[] textPalette;
    public Color ghostColor;
    int colourCounter = 0;
    int offsetCounter = 0;
    int charInterval = 50;
    public string[] texts;
    public string[] translations;
    public bool autogenerate = false;
    // 	#D81E05 flag red
    //  #0036B4 flag blue
    //  #007A3D flag green
    // 	#FDD116 flag yellow
    public static TextGenerator instance;
    private List<GameObject> targetLetters = new List<GameObject>();
    int totalLetters;
    public int foundLetters;
    int nextGPMove = 3;

    // Generate text in object spawner, then create the letters at the top


    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start () {
        if (autogenerate)
        {
            StartCoroutine(TextAutoGenerator());
        }

        offsetCounter = 50;

        //TargetTextGenerate("Sáivu");

        // StartCoroutine(GenerateNewText3d("SKáIMMADAS", GameObject.Find("TextTarget1").transform));
        //    StartCoroutine(GenerateNewText3d("SKáIMMADAS", new Vector2(10,-2)));
        // StartCoroutine(GenerateNewText3d("GULAHALLAT", this.transform));
        // StartCoroutine(GenerateNewText3d("TO TALK TO AND COMPREHEND NATURE", this.transform));

        // StartCoroutine(GenerateNewText("SKáIMMADAS", 150));
        // StartCoroutine(GenerateNewText("GULAHALLAT", 250, 500));
        // StartCoroutine(GenerateNewText("TO TALK TO AND COMPREHEND NATURE", 100, 400));

        Debug.Log(offsetCounter.ToString());
        // GenerateNewText("GAME", offsetCounter);

    }

    public void TargetTextGenerate(string s, string trans = "")
    {
        if (GameObject.Find("TargetText") == null)
        {
            // Only allow creation if the previous thing doesn't exist
            StartCoroutine(GenerateNewTargetText(s, GameObject.Find("TextTarget1").transform, trans));

        }
    }

    public void ClearTargetLetterList()
    {
        targetLetters.Clear();
    }

    public void RemoveTargetText()
    {
        try
        {
            Destroy(GameObject.Find("TargetText"));
        }
        catch
        {
            Debug.Log("Could not remove the target text object");
        }
    }

    public void HighlightLetter(string s)
    {
        if (s == "")
        {
            Debug.Log("Error: Trying to highlight a blank letter");
            return;

        }

        string s2 = s[0].ToString();

        foreach (GameObject g in targetLetters)
        {
            if(g.transform.GetComponent<TextMesh>().color.a < 1)
            {
                if(s2 == g.transform.GetComponent<TextMesh>().text)
                {
                    try
                    {

                        Color cl = g.transform.GetComponent<TextMesh>().color;
                        cl.a = 1;
                        g.transform.GetComponent<TextMesh>().color = cl;
                        foundLetters += 1;
                        if(foundLetters % nextGPMove == 0)
                        {
                            ZoneSpawner.Instance.MoveGoodPlace();
                            nextGPMove = Random.Range(2, 5);
                        }
                        
                    }
                    catch
                    {
                        Debug.Log("Could not highlight letter");
                    }
                    return;
                }
            }
        }
        
    }


    public IEnumerator TextAutoGenerator()
    {
        for (int i = 0; i < 10000; i++)
        {
            yield return new WaitForSeconds(Random.Range(3f, 6f));
            StartCoroutine(GenerateNewText3d(texts[Random.Range(0, texts.Length )], new Vector2(Random.Range(7, 8), Random.Range(-3f, 3f))));
        }
    }

    public IEnumerator GenerateNewText3d(string input, Transform target)
    {
        GameObject TextParent = new GameObject();
        float buffer = 0;
        TextParent.name = "TextParent";
        int i = 1;
        foreach (char c in input)
        {
            yield return new WaitForSeconds(Random.Range(0.1f, 0.15f));

            Transform t = Instantiate(textTemplate3d, new Vector3(target.transform.position.x + buffer, target.transform.position.y, -5), Quaternion.identity);
            t.GetComponent<TextMesh>().text = c.ToString();
            t.GetComponent<TextMesh>().color = textPalette[colourCounter];
            t.parent = TextParent.transform;
            if (c.ToString().ToUpper() == "W" || c.ToString().ToUpper() == "M")
            {
                buffer += 3f;
            }
            else
            {
                buffer += 2.5f;
            }
            colourCounter += 1;
            if (colourCounter >= textPalette.Length)
            {
                colourCounter = 0;
            }
            i += 1;
        }
    }

    public IEnumerator GenerateNewText3d(string input, Vector2 position)
    {
        GameObject TextParent = new GameObject();
        float buffer = 0;
        TextParent.name = "TextParent";
        int i = 1;
        foreach (char c in input)
        {
            yield return new WaitForSeconds(Random.Range(0.1f, 0.15f));

            Transform t = Instantiate(textTemplate3d, new Vector2(position.x + buffer, position.y), Quaternion.identity);
            t.GetComponent<TextMesh>().text = c.ToString();
            t.GetComponent<TextMesh>().color = textPalette[colourCounter];
            t.parent = TextParent.transform;

            buffer += 0.25f;

            colourCounter += 1;
            if (colourCounter >= textPalette.Length)
            {
                colourCounter = 0;
            }
            i += 1;
        }
    }

    public IEnumerator GenerateNewTargetText(string input, Transform target, string translation = "")
    {
        // Complete text plainly
        GameObject.Find("CompleteText").GetComponent<TextMesh>().text = translation;

        totalLetters = input.Length;
        foundLetters = 0;

        GameObject TextParent = new GameObject();
        float buffer = 0;
        TextParent.name = "TargetText";
        int i = 1;
        foreach (char c in input)
        {
            yield return new WaitForSeconds(Random.Range(0.1f, 0.15f));

            Transform t = Instantiate(targetText, new Vector3(target.transform.position.x + buffer, target.transform.position.y, 10), Quaternion.identity);
            t.GetComponent<TextMesh>().text = c.ToString();
            t.GetComponent<TextMesh>().color = ghostColor;
            t.parent = TextParent.transform;
            targetLetters.Add(t.gameObject);
            t.GetComponent<Renderer>().sortingOrder = 30;
            if (c.ToString().ToUpper() == "W" || c.ToString().ToUpper() == "M")
            {
                buffer += 3f;
            }
            else
            {
                buffer += 2.5f;
            }

            i += 1;
        }
    }
    public IEnumerator GenerateNewText(string input, int xOffset = 50, int yOffset = 800, bool bounce = false)
    {
        GameObject TextParent = new GameObject();
        int buffer = 0;
        TextParent.name = "TextParent";
        TextParent.transform.parent = GameObject.Find("Canvas").transform;
        int i = 1;
        
        foreach (char c in input)
        {
            yield return new WaitForSeconds(Random.Range(0.1f, 0.15f));
            Transform t = Instantiate(textTemplate, new Vector2(xOffset + buffer, yOffset), Quaternion.identity);
            t.parent = TextParent.transform;
            t.GetComponent<Text>().text = c.ToString();
            t.GetComponent<Text>().color = textPalette[colourCounter];
            offsetCounter += charInterval;
            if (offsetCounter > 1800)
            {
                offsetCounter = 0;
            }
            charInterval = CheckFontWidth(c);
            buffer += charInterval;
         

            colourCounter += 1;
            if (colourCounter >= textPalette.Length)
            {
                colourCounter = 0;
            }
            i += 1;

        }
    }

    int CheckFontWidth(char c)
    {
        string s = "MW";
        int interval = 50;
        s = s.ToUpper();
        if(s.Contains(c.ToString()))
        {
            interval = 50;
            Debug.Log("Large gap");
        } else
        {
            interval = 35;
        }
        return interval;
    }

	// Update is called once per frame
	void Update () {
		
	}

    public void DisplayLevelText(string s)
    {
        levelText.transform.GetComponent<TextMesh>().text = s;
        // Set contents at text generation phase
        Sequence wordSequence = DOTween.Sequence();
        wordSequence.Append(levelText.transform.DOMove(new Vector3(58.3f,50,20), 2));
        wordSequence.Append(levelText.transform.DOMove(new Vector3(58.3f, 50, 20), 2.5f));
        wordSequence.Append(levelText.transform.DOMove(new Vector3(58.3f, -100, 0), 2f));
    }

}
