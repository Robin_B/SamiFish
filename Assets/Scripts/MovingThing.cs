﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class MovingThing : MonoBehaviour
{
    private int particleId = 0;
    private bool firstUpdate = true;
    private bool attraction = false;
    public Renderer myRenderer;
    private Vector3 attractionDir;
    private int checkFishIndex = 0;
    public static int fishCount;
    public static int liveFish;

    private int hp = 4;
    private int startHP = 4;
    public bool alive = true;
    public Transform collisionEffect;
    public Transform happyEffect;

    private float scaleFactor = 72.0f / 50.0f;
    private Color stableColor;

    private Color originalColor;
    private float stabilizer = 1.0f;

    public bool letter = false;
    public string myLetter = "";
    public Color[] letterColours;

    public Material myDeathParticle;
    public Transform deathEffect;
    private float resetSpriteTimer;

    void Start()
    {

        if (!letter)
        {
            fishCount += 1;
            stableColor = transform.Find("BodySprite").GetComponent<SpriteRenderer>().color;
            originalColor = stableColor;
            checkFishIndex = Random.Range(0, 200);
            hp = startHP;
            resetSpriteTimer = Random.Range(3.1f, 3.2f);
        } else
        {
            transform.Find("FishText").GetComponent<TextMesh>().color = letterColours[Random.Range(0, letterColours.Length)];
               
            try
            {
                transform.Find("FishText").GetComponent<Renderer>().sortingOrder = 30;
            }
            catch
            {

            }
        }
    }

    public void SetLetter(string s)
    {
        transform.Find("FishText").GetComponent<TextMesh>().text = s;
        myLetter = s;
        // hide other sprites
        Color fade = new Color(1, 1, 1, 0);
        transform.Find("BodySprite").GetComponent<SpriteRenderer>().color = fade;
        transform.Find("SkeletonSprite").GetComponent<SpriteRenderer>().color = fade;

    }

    public void Init(int myParticleId)
    {
        particleId = myParticleId;
    }

    void Update()
    {
        if(!letter)
        {
            resetSpriteTimer -= Time.deltaTime;
            if(resetSpriteTimer<0)
            {
                ResetSprite();
                resetSpriteTimer = Random.Range(3.1f, 3.2f);
            }
        }

        if (VelocityFieldFish.instance.particles.Length > particleId)
        {
            if (firstUpdate)
            {
                VelocityFieldFish.instance.particles[particleId].isMovingThing = true;
                firstUpdate = false;
            }
            var _origPos = this.transform.position;
            Vector3 targetPos = new Vector3();
            if (alive)
            {
                targetPos = new Vector3(VelocityFieldFish.instance.particles[particleId].x * scaleFactor, VelocityFieldFish.instance.particles[particleId].y * scaleFactor, 0);
            }
            else
            {
                float yLoss = -0.1f;
                if(transform.position.y < 0.8f)
                {
                    yLoss = 0;
                    targetPos = new Vector3(transform.position.x, transform.position.y + yLoss, 0);

                }
                else if (transform.position.y < 2f)
                {
                    targetPos = new Vector3(transform.position.x, transform.position.y + yLoss, 0);
                } else
                {
                    yLoss = -0.3f;
                    targetPos = new Vector3(VelocityFieldFish.instance.particles[particleId].x * scaleFactor, VelocityFieldFish.instance.particles[particleId].y * scaleFactor + yLoss, 0);
                }
                SetPosition(targetPos);
            }
            if (attraction)
            {
                SetPosition(targetPos + attractionDir);
                attraction = false;
                attractionDir = Vector3.zero;
                ;
            }

            if (Random.Range(0, 100) > 80)
            {
                // check distance to fish with index checkFishIndex, and increase if too close
                checkFishIndex++;
                if (checkFishIndex >= ObjectSpawner.instance.spawnedObjects.Count) checkFishIndex = 0;
                if (checkFishIndex != particleId)
                {
                    float distance = Vector3.Distance(this.transform.position, ObjectSpawner.instance.spawnedObjects[checkFishIndex].transform.position);
                    if (distance < 1)
                    {
                        SetPosition(targetPos + new Vector3(Random.Range(-10.0f, 10.0f), Random.Range(-10.0f, 10.0f), 0));
                    }
                }
            }
            


            Vector3 moveDirection = targetPos - _origPos;
            if (moveDirection != Vector3.zero)
            {
                float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.AngleAxis(angle, Vector3.forward),
                    0.1f);
            }

            this.transform.position = Vector3.Lerp(this.transform.position, targetPos, 0.1f);
        }
    }

    public void SetAttractor(Vector3 dir)
    {
        attraction = true;
        attractionDir = dir;
    }

    public void SetPosition(Vector3 pos)
    {
        VelocityFieldFish.instance.particles[particleId].x = pos.x / scaleFactor;
        VelocityFieldFish.instance.particles[particleId].y = pos.y / scaleFactor;
       // this.transform.position = pos;
    }

    void FlashRed()
    {

    }

    public void EnterThreat(Threat threat)
    {

        // Letters don't get damaged
        if (!letter)
        {

                if (threat.transform.tag == "Threat")
                {
                    Instantiate(collisionEffect, new Vector3(threat.transform.position.x, threat.transform.position.y, 10), Quaternion.identity);
                    //SFX

                    //Debug.Log("Moving Object entered Threat!");
                    hp -= 1;
                    // Fade skeleton into view, proportional to hp loss
                    float transparencyLoss = ((float)hp / (float)startHP);
                    Color newColor = transform.Find("BodySprite").GetComponent<SpriteRenderer>().color;
                    stableColor = newColor;
                    newColor.a = transparencyLoss;
                    transform.Find("BodySprite").GetComponent<SpriteRenderer>().color = newColor;

                    Sequence flashRed = DOTween.Sequence();
                    flashRed.Append(transform.Find("BodySprite").GetComponent<SpriteRenderer>().DOColor(Color.red, 0.3f));
                    flashRed.Append(transform.Find("BodySprite").GetComponent<SpriteRenderer>().DOColor(newColor, 0.3f));
                    flashRed.Play();



                    if (hp <= 0)
                    {
                    if (alive)
                    {
                        Die();
                    }
                    }

                }
                if (threat.transform.tag == "GoodPlace")
                {
                if (Random.Range(1, 10) < 5)
                {

                    hp += 1;
                    Sequence flashBlue = DOTween.Sequence();
                    Color newColor = transform.Find("BodySprite").GetComponent<SpriteRenderer>().color;
                    flashBlue.Append(transform.Find("BodySprite").GetComponent<SpriteRenderer>().DOColor(Color.cyan, 0.6f));
                    flashBlue.Append(transform.Find("BodySprite").GetComponent<SpriteRenderer>().DOColor(Color.green, 0.3f));
                    flashBlue.Append(transform.Find("BodySprite").GetComponent<SpriteRenderer>().DOColor(newColor, 0.3f));
                    flashBlue.Play();
                }
                }
            
        }
        else if (letter && myLetter != "")
        {
            if (threat.transform.tag == "GoodPlace")
            {
                try 
                {
                    Instantiate(happyEffect, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity);

                    string l = myLetter;
                    SetLetter("");
                    TextGenerator.instance.HighlightLetter(l);
                    ObjectSpawner.instance.CheckWordComplete();

                    AudioController.instance.PlayEffect(Random.Range(11, 13));

                    Debug.Log("Letter hit!");
                }
                catch
                {
                    Debug.Log("Could not remove letter");
                }
                // The letter has reached its goal and forms a part of the aimed for word
                // Remove from circulation and highlight in top text
                
            }
        }
    }

    void Die()
    {
        alive = false;
        if (Random.Range(1, 10) < 5)
        {
            int deathSound = Random.Range(7, 9);
            AudioController.instance.PlayEffect(deathSound, 2);
        }
        ObjectSpawner.instance.FishDied();
        Transform death = Instantiate(deathEffect, this.transform.position, Quaternion.identity);
        death.GetComponent<ParticleSystem>().GetComponent<Renderer>().material = myDeathParticle;
        transform.Find("SkeletonSprite").GetComponent<SpriteRenderer>().sortingOrder = 20;
    }

    void ResetSprite()
    {
        // Sprite should be by default just the normal fish sprite with no fading
        //transform.Find("BodySprite").GetComponent<SpriteRenderer>().color = originalColor;

        // Then for each HP it should fade by hp / total hp 
        float transparencyLoss = ((float)hp / (float)startHP);


        Color c = new Color(originalColor.r, originalColor.b, originalColor.b, transparencyLoss);

        transform.Find("BodySprite").GetComponent<SpriteRenderer>().color = c;
    }

    public void ReviveFish()
    {
        alive = true;
        hp = startHP;
        transform.Find("BodySprite").GetComponent<SpriteRenderer>().color = originalColor;
    }

}
