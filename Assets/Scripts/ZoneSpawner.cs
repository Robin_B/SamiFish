﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class ZoneSpawner : MonoBehaviour
{

    // Use Clear and Create

    private static ZoneSpawner _instance;
    private bool firstCreation = true;
    public static ZoneSpawner Instance { get { return _instance; } }

    // Spawns threat areas and a healing place
    public GameObject[] threats;
    public GameObject goodPlace;
    public GameObject spawnZone;
    public GameObject goodPlaceInstance;
    Zone targetZoneGP;
    int xBuffer = 15;
    int yBuffer = 55;
    int side = 21;
    int spawnSide = 14;

    private bool creating = false;
   
    private List<Zone> spawnZones = new List<Zone>();


    private class Zone: ZoneSpawner
    {
        public int id;
        public int x, y;
        public bool occupied = false;
        public GameObject zoneObj;
        // Each zone could have it's own little pitch for the sound effects
        public float basePitch;
    }

    private void Awake()
    {
        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else
        {
            _instance = this;
        }
    }


    void Start()
    {

        CreateSpawnGrid();

    }

    // Split play area into a grid of 5 by 3 and check for overlaps
    void CreateSpawnGrid()
    {
        if (GridEmpty())
        {


            int id = 1;
            for (int y = 0; y < 3; y++)
            {
                for (int x = 0; x < 5; x++)
                {
                    id++;
                    GameObject s = Instantiate(spawnZone, new Vector3(xBuffer + x * side, yBuffer - y * side, -20), Quaternion.identity);
                    Zone z = new Zone();
                    z.x = x;
                    z.y = y;
                    z.id = id;
                    z.basePitch = 1.0f / (float)id;
                    spawnZones.Add(z);
                }
            }
        }

    }
    private bool GridEmpty()
    {
        for (int i = 0; i < spawnZones.Count; i++)
        {
            if (spawnZones[i].occupied == true)
            {
                return false;
            }
        }
        return true;
    }

    Zone PickFreeZone()
    {
        bool freeZones = false;
        for (int i = 0; i < spawnZones.Count; i++)
        {
            if(spawnZones[i].occupied == false)
            {
                freeZones = true;
            }
        }
        if (!freeZones) return null;

        int t = 1000;
        while(t > 0)
        {
            t -= 1;
            Zone result = spawnZones[Random.Range(0, spawnZones.Count)];
            if (!result.occupied)
            {
                return result;
            }
        }
        return null;
    }

    public void CreateStage(int threatAmount, float spawnDelay = 0.6f)
    {
// creating = true;
        /*
        string chosenWord = TextGenerator.instance.texts[Random.Range(0, TextGenerator.instance.texts.Length)];
        // Create letters at the top
        TextGenerator.instance.TargetTextGenerate(chosenWord);
        // Create letters in the field
        ObjectSpawner.instance.AddLetters(chosenWord);
        // Create threats and the good place
        StartCoroutine(CreateStageCoroutine(threatAmount, spawnDelay));

        StartCoroutine(EndCreation());*/
    }

    public IEnumerator EndCreation()
    {
        yield return new WaitForSeconds(5.0f);
        creating = false;
    }
    public IEnumerator CreateStageCoroutine(int threatAmount, float spawnDelay = 2.0f)
    {

        for (int i = 0; i < threatAmount; i++)
        {
            yield return new WaitForSeconds(spawnDelay);
            // pick random available zone
            SpawnThreat();
            AudioController.instance.PlayEffect(5);
                
        }
        yield return new WaitForSeconds(spawnDelay);

        // pick random available zone
        targetZoneGP = PickFreeZone();

        float xPosGP = Random.Range(xBuffer - 10 + targetZoneGP.x * side, xBuffer + targetZoneGP.x * side + spawnSide);
        float yPosGP = Random.Range(yBuffer + 5 - targetZoneGP.y * side, yBuffer - 5 - targetZoneGP.y * side);

        GameObject gp = Instantiate(goodPlace, new Vector2(xPosGP, yPosGP), Quaternion.identity);
        goodPlaceInstance = gp;
        gp.transform.DOScale(Random.Range(0.4f, 0.6f), Random.Range(1, 3));
        targetZoneGP.occupied = true;
        targetZoneGP.zoneObj = gp;

        AudioController.instance.PlayEffect(3);

    }
    
    public void MoveGoodPlace()
    {
        // Pick new free location
        Zone newZone = PickFreeZone();

        // Free up current location 
        targetZoneGP.occupied = false;
        targetZoneGP.zoneObj = null;

        // Move to new location
        targetZoneGP = newZone;
        targetZoneGP.occupied = true;
        targetZoneGP.zoneObj = goodPlaceInstance;
        float xPosGP = Random.Range(xBuffer - 10 + targetZoneGP.x * side, xBuffer + targetZoneGP.x * side + spawnSide);
        float yPosGP = Random.Range(yBuffer + 5 - targetZoneGP.y * side, yBuffer - 5 - targetZoneGP.y * side);

        Vector3 newPos = new Vector3(xPosGP, yPosGP, 0);
        targetZoneGP.zoneObj.transform.DOMove(newPos, 2.0f);
        
    }

    // Random placement of threats
    public void SpawnThreat()
    {
        // pick random available zone
        Zone targetZone = PickFreeZone();

        float xPos = Random.Range(xBuffer - 10 + targetZone.x * side, xBuffer + targetZone.x * side + spawnSide);
        float yPos = Random.Range(yBuffer + 5 - targetZone.y * side, yBuffer - 5 - targetZone.y * side);

        GameObject g = Instantiate(threats[Random.Range(0, threats.Length)], new Vector2(xPos, yPos), Quaternion.identity);
        g.transform.DOScale(Random.Range(0.8f, 1.2f), Random.Range(1, 3));
        targetZone.occupied = true;
        targetZone.zoneObj = g;
    }

    public void SpawnGoodPlace()
    {
        // pick random available zone
        Zone targetZone = PickFreeZone();

        float xPos = Random.Range(xBuffer - 10 + targetZone.x * side, xBuffer + targetZone.x * side + spawnSide);
        float yPos = Random.Range(yBuffer + 5 - targetZone.y * side, yBuffer - 5 - targetZone.y * side);

        GameObject g = Instantiate(goodPlace, new Vector2(xPos, yPos), Quaternion.identity);
        g.transform.DOScale(Random.Range(0.4f, 0.6f), Random.Range(1, 3));
        targetZone.occupied = true;
        targetZone.zoneObj = g;
    }


    public void ClearZones()
    {
        StartCoroutine(ClearZonesCoroutine());
    }

    public void ClearAndCreate(int threatAmount = 2, float spawnDelay = 0.5f, string chosenWord = "RANDOM", string chosenWordTranslation = "RANDOM")
    {
        StartCoroutine(ClearAndCreateCoroutine(threatAmount, spawnDelay, chosenWord, chosenWordTranslation));
    }

    public IEnumerator ClearZonesCoroutine()
    {
        for (int i = 0; i < spawnZones.Count; i++)
        {
            yield return new WaitForSeconds(0.3f);
            if (spawnZones[i].zoneObj != null)
            {
                spawnZones[i].zoneObj.GetComponent<Disappear>().Do();
                spawnZones[i].zoneObj = null;
                spawnZones[i].occupied = false;
            }
        }
    }

    public IEnumerator ClearAndCreateCoroutine(int threatAmount = 2, float spawnDelay = 0.5f, string chosenWord = "RANDOM", string chosenWordTranslation = "RANDOM")
    {
        if (!creating)
        {
            creating = true;

            if (!firstCreation)
            {
                // Clear existing things
                for (int i = 0; i < spawnZones.Count; i++)
                {
                    yield return new WaitForSeconds(0.3f);
                    if (spawnZones[i].zoneObj != null)
                    {
                        spawnZones[i].zoneObj.GetComponent<Disappear>().Do();
                        spawnZones[i].zoneObj = null;
                        spawnZones[i].occupied = false;
                    }
                }

                // Clear text objects
                TextGenerator.instance.RemoveTargetText();

                // Remove any stray letters
                ObjectSpawner.instance.ClearLetters();

                yield return new WaitForSeconds(0.5f);
            }

            firstCreation = false;
            // Start creating anew

            if (chosenWord == "RANDOM")
            {
                int r = Random.Range(0, TextGenerator.instance.texts.Length);
                chosenWord = TextGenerator.instance.texts[r];
                chosenWordTranslation = TextGenerator.instance.translations[r];
            } else if (chosenWord != "RANDOM" && chosenWordTranslation == "RANDOM")
            {
                // if the main word isn't random, just match it
                chosenWordTranslation = chosenWord;
            }

            yield return new WaitForSeconds(0.2f);

            // Create letters at the top
            TextGenerator.instance.TargetTextGenerate(chosenWord, chosenWordTranslation);
            // Create letters in the field
            ObjectSpawner.instance.AddLetters(chosenWord);

            for (int i = 0; i < threatAmount; i++)
            {
                yield return new WaitForSeconds(spawnDelay);
                // pick random available zone
                SpawnThreat();
                AudioController.instance.PlayEffect(5);

            }
            yield return new WaitForSeconds(spawnDelay);

            // pick random available zone
            targetZoneGP = PickFreeZone();

            float xPosGP = Random.Range(xBuffer - 10 + targetZoneGP.x * side, xBuffer + targetZoneGP.x * side + spawnSide);
            float yPosGP = Random.Range(yBuffer + 5 - targetZoneGP.y * side, yBuffer - 5 - targetZoneGP.y * side);

            GameObject gp = Instantiate(goodPlace, new Vector2(xPosGP, yPosGP), Quaternion.identity);
            //gp.transform.DOScale(Random.Range(0.4f, 0.6f), Random.Range(1, 3));
            goodPlaceInstance = gp;
            Sequence gpseq = DOTween.Sequence();
            float originalScale = gp.transform.localScale.x;
            gpseq.Append(gp.transform.DOScale(originalScale + 0.06f, 2f));
            gpseq.Append(gp.transform.DOScale(originalScale, 0.6f));
            gpseq.Append(gp.transform.DOScale(originalScale + 0.03f, 2f));
            gpseq.Append(gp.transform.DOScale(originalScale, 0.3f));

            gpseq.SetLoops(-1, LoopType.Restart);
            gpseq.Play();

            targetZoneGP.occupied = true;
            targetZoneGP.zoneObj = gp;

            AudioController.instance.PlayEffect(3);

            creating = false;
        }
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.D))
        {
            StartCoroutine(ClearZonesCoroutine());
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            if (!creating)
            {
                ClearAndCreate();
            }
        }
        if(Input.GetKeyDown(KeyCode.M))
        {
            MoveGoodPlace();
        }
    }

    public bool StageComplete()
    {
        // Returns true when all the letters have been found
        // But not during stage creation
        if (!creating)
        {
            return (ObjectSpawner.instance.LettersCollected());
        }
        return false;
    }
}
