﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObjectSpawner : MonoBehaviour
{

    public MovingThing objectToSpawn;
    public int initialObjects = 20;
    public Sprite[] fishes;
    public Sprite[] skeletons;
    public Material[] deathParticles;
    public GameObject completeText;

    string activeWord = "";

    public List<MovingThing> spawnedObjects = new List<MovingThing>();
    public List<MovingThing> spawnedLetters = new List<MovingThing>();

    public static ObjectSpawner instance;
    bool busy = false;

    public int totalAliveFish = 0;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        for (int i = 0; i < initialObjects; i++)
        {
            var thing = Instantiate(objectToSpawn, new Vector3(Random.Range(1, 127), Random.Range(1, 71)),
                Quaternion.identity);
            thing.Init(i);
            spawnedObjects.Add(thing);

            // The first 15 fish are actually immortal placeholders for letters
            if (i < 15)
            {
                thing.letter = true;
            }

            // match skeleton sprite with fish sprite
            int r = Random.Range(0, fishes.Length);
            thing.transform.Find("BodySprite").GetComponent<SpriteRenderer>().sprite = fishes[r];
            thing.transform.Find("SkeletonSprite").GetComponent<SpriteRenderer>().sprite = skeletons[r];
            thing.transform.GetComponent<MovingThing>().myDeathParticle = deathParticles[r];

        }

        totalAliveFish = initialObjects - 15;
    }

    public void AddLetters(string s)
    {
        if (activeWord == "")
        {
            activeWord = s;
        }
        else
        {
            // Can't set new word unless previous one is complete
            Debug.Log("Tried to add letters but an existing word is active");
            return;
        }

        int extent = s.Length;
        for (int i = 0; i < extent; i++)
        {

            spawnedObjects[i].SetLetter(s[i].ToString());
        }

        // Create the target view up top
        TextGenerator.instance.TargetTextGenerate(s);

    }

    public void ClearLetters()
    {
        // Removes floating letters from the play area
        for (int i = spawnedObjects.Count - 1; i >= 0;  i--)
        {
            if(spawnedObjects[i].letter)
            {
                try
                {

                    spawnedObjects[i].SetLetter("");
                    spawnedObjects[i].myLetter = "";
                    AudioController.instance.PlayEffect(Random.Range(11, 14));

                }
                catch
                {
                    Debug.Log("Could not remove it");
                }
            }
        }
        activeWord = "";
    }

    public void CheckWordComplete()
    {
        bool empty = true;

        // Check if all letters have been removed
        for (int i = spawnedObjects.Count - 1; i >= 0; i--)
        {
            if (spawnedObjects[i].myLetter != "")
            {
                empty = false;
            }
        }
        if (empty)
        {
            activeWord = "";
            TextGenerator.instance.ClearTargetLetterList();
            busy = true;
            // Set contents at text generation phase
            Sequence wordSequence = DOTween.Sequence();
            wordSequence.Append(completeText.transform.DOMove(Vector3.zero, 2));
            wordSequence.Append(completeText.transform.DOMove(Vector3.zero, 5));
            wordSequence.Append(completeText.transform.DOMove(new Vector3(0, -100, 0), 2));
            wordSequence.AppendCallback(SetNotBusy);
            wordSequence.Play();

            Debug.Log("Word complete!");
            AudioController.instance.PlayEffect(10);

        }

    }

    public void RemoveLetter(string s)
    {
        try
        {

            s = s[0].ToString();
            for (int i = spawnedObjects.Count - 1; i >= 0; i--)
            {
                if (spawnedObjects[i].myLetter == s)
                {
                    try
                    {
                        
                        AudioController.instance.PlayEffect(Random.Range(11, 14));
                        TextGenerator.instance.HighlightLetter(s);
                        // Set the corresponding letter in the top word
                        break;
                    }
                    catch
                    {
                        Debug.Log("Could not remove letter from spawn object number " + i.ToString());
                    }
                }
            }
        }
        catch
        {

        }
            bool empty = true;

            // Check if all letters have been removed
            for (int i = spawnedObjects.Count - 1; i >= 0; i--)
            {
                if (spawnedObjects[i].myLetter != "")
                {

                    empty = false;
                }
            }
            if (empty)
            {
                activeWord = "";
                busy = true;
                // Set contents at text generation phase
                Sequence wordSequence = DOTween.Sequence();
                wordSequence.Append(completeText.transform.DOMove(Vector3.zero, 2));
                wordSequence.Append(completeText.transform.DOMove(Vector3.zero, 5));
                wordSequence.Append(completeText.transform.DOMove(new Vector3(0, -100, 0), 2));
                wordSequence.AppendCallback(SetNotBusy);
                wordSequence.Play();

                Debug.Log("Word complete!");
                AudioController.instance.PlayEffect(10);
              
            }
        
    }

    void SetNotBusy()
    {
        busy = false;
    }

    public bool LettersCollected()
    {
        bool empty = true;
        
        // Check if all letters have been removed abd nothing else nasty is happening
            for (int i = spawnedObjects.Count - 1; i >= 0; i--)
            {
                if (spawnedObjects[i].myLetter != "")
                {
                    empty = false;
                }
            }

        if (!busy)
        {
            //Debug.Log("The letters have been collected: " + empty.ToString());
            return empty;
        } else
        {
            return false;
        }
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Z))
        {
            ClearLetters();
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            AddLetters("SKÁIMADDAS");
        }
    }


    internal void ReviveFish()
    {
        Debug.Log("Reviving fish. Alive before: " + totalAliveFish);
        for (int i = 15; i < spawnedObjects.Count; i++)
        {
            if (!spawnedObjects[i].alive)
            {
                spawnedObjects[i].ReviveFish();
                totalAliveFish++;
            }
        }
        Debug.Log("Reviving fish. Alive after: " + totalAliveFish);
    }

    internal void FishDied()
    {
        totalAliveFish--;
    }
}
