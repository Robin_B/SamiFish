﻿using UnityEngine;
using System.Collections;
public class WaterParticles : MonoBehaviour
{
    ParticleSystem.Particle[] cloud;
    public ParticleSystem particleSystem;
    bool bPointsUpdated = false;
    private float scaleFactor = 72.0f / 50.0f;

    void Start()
    {
    }

    void Update()
    {
        SetPoints();
        if (bPointsUpdated)
        {
            particleSystem.SetParticles(cloud, cloud.Length);
            bPointsUpdated = false;
        }
    }

    public void SetPoints()
    {
        float particleMaxSize = 0.5f;
        cloud = new ParticleSystem.Particle[VelocityFieldFish.instance.particles.Length];
        //Debug.Log("Setting " + cloud.Length + " particles.");
        for (int ii = ObjectSpawner.instance.spawnedObjects.Count; ii < cloud.Length; ++ii)
        {
            //cloud[ii].position = positions[ii];
            cloud[ii].position = new Vector3(VelocityFieldFish.instance.particles[ii].x * scaleFactor,
                VelocityFieldFish.instance.particles[ii].y * scaleFactor, -1);

            if (VelocityFieldFish.instance.particles[ii].age < 1)
                cloud[ii].startSize = VelocityFieldFish.instance.particles[ii].age * particleMaxSize;
            else if (VelocityFieldFish.instance.particles[ii].timer < 1)
                cloud[ii].startSize = VelocityFieldFish.instance.particles[ii].timer * particleMaxSize;
            else
                cloud[ii].startSize = particleMaxSize;

            //cloud[ii].remainingLifetime = VelocityFieldFish.instance.particles[ii].timer;
        }

        bPointsUpdated = true;
    }
}