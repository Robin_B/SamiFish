﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private static int STAGE_INIT = 0;
    private static int STAGE_INIT2 = 10;
    private static int STAGE_START = 1;
    private static int STAGE_PLAYING = 2;
    private static int STAGE_LOST = 3;
    private static int STAGE_WON = 4;

    private int stage = STAGE_INIT;
    private int level = 0;
    private float gameTimer = 0;
    private float timeSinceLastInteraction = 0;
    private int numLevels = 4;

    public bool playerHasTouched = false;
    private int lastSwoosh = 0;
    public float resetTimeInSeconds = 60;
    public bool zonePlayComplete = false;
    private bool zoneLyricsComplete = false;

    public Transform introText, introText2;

    public ZoneSpawner zoneSpawner;
    private int angryPhases = 0;
    private bool isAngry = false;

    public Transform winPage;

    void Start()
    {
        playerHasTouched = false;
    }

    void WinGame()
    {
        stage = STAGE_WON;
        Skaimmadas.instance.PlayMe();
        AudioController.instance.StartWin();
    }

    void CheckStage()
    {
        if (stage == STAGE_INIT)
        {
            // initial stage - start screen
            Skaimmadas.instance.StartScreen();
            AudioController.instance.StartScreen();

            // intro
            introText.DOMove(Vector3.zero, 2);

            stage = STAGE_INIT2;
            return;
        }
        if (stage == STAGE_INIT2 && playerHasTouched)
        {
            if (gameTimer > 5)
            {
                introText.DOMove(new Vector3(0, -100, 0), 2);
                introText2.DOMove(Vector3.zero, 1);
                stage = STAGE_START;
                playerHasTouched = false;
                return;
            }
            else
            {
                playerHasTouched = false;
            }
        }

        if (stage == STAGE_START && playerHasTouched)
        {
            if (gameTimer > 8)
            {

                // start game proper
                Debug.Log("Starting game");
                stage = STAGE_PLAYING;
                level = 1;
                Skaimmadas.instance.MainGame();
                introText2.DOMove(new Vector3(0, -100, 0), 2);

                zoneSpawner.ClearAndCreate(0);
                AudioController.instance.NextStage(level);
                TextGenerator.instance.DisplayLevelText("Level " + (level).ToString() + "\nGuide the letters into the swirl!");
                return;
            }
            else
            {
                playerHasTouched = false; // ignore earlier touches
            }
        }


        if (stage == STAGE_PLAYING)  // zone complete? then go to next stage
        {
            if (IsZoneComplete())
            {
                AudioController.instance.PlayEffect(9);
                level++;
                Debug.Log("Next Level! Now level " + level);
                zoneLyricsComplete = false;
                zonePlayComplete = false;
                if (level >= numLevels)
                {
                    // game won!
                    WinGame();
                    winPage.DOMove(Vector3.zero, 2);

                }
                else
                {
                    Invoke("InvokeNextLevel", 5);
                }
            }

            // check if Skaimmadas should get angry
            if (ObjectSpawner.instance.totalAliveFish < 40 && !isAngry)
            {
                StartAngryPhase();
            }

            if (isAngry && AudioController.instance.GetLyricsProgress() > 0.995f)
            {
                // end angry
                EndAngryPhase();
            }

        }

        if (stage == STAGE_WON)
        {
            // check for tap on top left
            if (Input.touchCount == 1)
            {
                var pos = Camera.main.ScreenToWorldPoint(Input.touches[0].position);
                Debug.Log("Touch pos" + pos);
                Debug.Log("Mouse pos" + pos);
                if (pos.x > 0.85f && pos.z > 0.25f)
                {
                    Application.LoadLevel(Application.loadedLevel);
                }
            }
            if (Input.GetMouseButtonDown(0))
            {
                var pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Debug.Log("Mouse pos" + pos);
                if (pos.x > 0.85f && pos.z > 0.25f)
                {
                    Application.LoadLevel(Application.loadedLevel);
                }
                ;
            }
        }
    }

    private void InvokeNextLevel()
    {
        // Display next level text
        TextGenerator.instance.DisplayLevelText("Level " + level.ToString() + "\nGuide the letters into the swirl\nBut don't let the fish fall into the red danger areas!");

        // Pass the next word as the target
        string nextWord = TextGenerator.instance.texts[level];
        zoneSpawner.ClearAndCreate(level);

     
        AudioController.instance.NextStage(level);
    }

    public void PlayerCompletedWord()
    {
        if (zoneLyricsComplete) zonePlayComplete = true;
    }

    private bool IsZoneComplete()
    {
        //Debug.Log("Progress: " + AudioController.instance.GetLyricsProgress());
        if (!zoneLyricsComplete && AudioController.instance.GetLyricsProgress() > 0.995f)
        {
            zoneLyricsComplete = true;
        }

        if (!zoneLyricsComplete)
        {
            if (zoneSpawner.StageComplete())
            {
                Debug.Log("Stage is complete, but lyrics are still playing! Lyrics completed: " + AudioController.instance.GetLyricsProgress());
                if (AudioController.instance.GetLyricsProgress() < 0.9f)
                {
                    // still some time left, do another text challenge
                    //zoneSpawner.ClearZones();
                    if (level == 1)
                        zoneSpawner.ClearAndCreate(0);
                    else
                        zoneSpawner.ClearAndCreate(level);

                }
                else zonePlayComplete = true;
            }

        }
        // hack so the stage complete doesn't trigger too often
        if (AudioController.instance.GetLyricsProgress() > 0.1f &&
            AudioController.instance.GetLyricsProgress() < 0.5f)
            zonePlayComplete = true;

        if (zoneSpawner.StageComplete() && zoneLyricsComplete && zonePlayComplete)
        {
            return true;
        }

        return false;
    }

    public void StartAngryPhase()
    {
        // move in good old Skaimmadas!
        isAngry = true;
        Skaimmadas.instance.StartShouting();
        angryPhases++;
        Invoke("StartAngryAudio", 2);
        Invoke("ReviveFish", 5);
    }

    public void EndAngryPhase()
    {
        isAngry = false;
        Skaimmadas.instance.MainGame();
    }

    void ReviveFish()
    {
        ObjectSpawner.instance.ReviveFish();
    }

    void StartAngryAudio()
    {
        AudioController.instance.StartShouting(angryPhases);
    }

    void Update()
    {

        CheckStage();

        gameTimer += Time.deltaTime;
        timeSinceLastInteraction += Time.deltaTime;

        if (timeSinceLastInteraction > resetTimeInSeconds || Input.touchCount > 10)
        {
            // restart game
            Application.LoadLevel(Application.loadedLevel);
        }

        if (Input.touchCount > 0 || Input.GetMouseButton(0))
        {
            if (timeSinceLastInteraction > 0.1f || !playerHasTouched)
            {
                int nextSwoosh = Random.Range(2, 6);
                while (nextSwoosh == lastSwoosh)
                {
                    nextSwoosh = Random.Range(2, 6);
                }
                lastSwoosh = nextSwoosh;
                AudioController.instance.PlayEffect(nextSwoosh, 0.5f);
                
            }
            playerHasTouched = true;
            timeSinceLastInteraction = 0;
        }

        if (Input.GetKeyDown(KeyCode.A)) StartAngryPhase();
        if (Input.GetKeyDown(KeyCode.N)) zonePlayComplete = true;
        if (Input.GetKeyDown(KeyCode.W)) WinGame();
    }
}
