﻿using UnityEngine;
using System.Collections;

public class MicInput : MonoBehaviour
{

    public float testSound;
    public static float MicLoudness;
    private string _device;
    private AudioClip _clipRecord = new AudioClip();
    private int _sampleWindow = 128;
    private bool _isInitialized;
    private float overalPeak = 0.000000000001f;
    public Transform rotatingPiece;

    void InitMic()
    {
        if (_device == null)
        {
            _device = Microphone.devices[0];
            _clipRecord = Microphone.Start(_device, true, 999, 44100);
            Debug.Log(_clipRecord);
        }
    }

    void StopMicrophone()
    {
        Microphone.End(_device);
    }

    float LevelMax()
    {
        float levelMax = 0;
        float[] waveData = new float[_sampleWindow];
        int micPosition = Microphone.GetPosition(null) - (_sampleWindow + 1);
        if (micPosition < 0)
        {
            return 0;
        }
        _clipRecord.GetData(waveData, micPosition);
        for (int i = 0; i < _sampleWindow; ++i)
        {
            float wavePeak = waveData[i] * waveData[i];
            if (levelMax < wavePeak)
            {
                levelMax = wavePeak;
            }
        }
        return levelMax;
    }

    void Update()
    {
        MicLoudness = LevelMax();
        testSound = MicLoudness;
        if (MicLoudness > overalPeak) overalPeak = MicLoudness;
        float angle = 90.0f * MicLoudness / overalPeak;
        if (overalPeak > 0.000000000001f) overalPeak *= 0.95f;
        if (angle > 65.0f) angle = 65;
        rotatingPiece.localEulerAngles = Vector3.Lerp(rotatingPiece.localEulerAngles, new Vector3(0,0, 65.0f - angle), 0.2f);
    }

    void OnEnable()
    {
        InitMic();
        _isInitialized = true;
    }

    void OnDisable()
    {
        StopMicrophone();
    }

    void OnDestory()
    {
        StopMicrophone();
    }

    void OnApplicationFocus(bool focus)
    {
        if (focus)
        {
            if (!_isInitialized)
            {
                InitMic();
                _isInitialized = true;
            }
        }

        if (!focus)
        {
            StopMicrophone();
            _isInitialized = false;
        }
    }

}