﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Threat : MonoBehaviour
{

    public bool moving = false;
    public float speed = 2.0f;
    public float rotationSpeed = 5.0f;

    public float maxAttractDistance = 10;
    public float attractionStrength = 1.0f;

    public bool gp;

    void Start()
    {
        rotationSpeed = Random.Range(3, 8);
        if(Random.Range(0,10) < 5)
        {
            rotationSpeed = -rotationSpeed;
        }
        transform.Rotate(Vector3.forward * Random.Range(-90,90));
    }

    void Update()
    {
        // move threat if it's a moving object 
        if(moving)
        {
            transform.Translate(Vector2.left * speed * Time.deltaTime);
        }

        // Rotate for show
        transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed);

        // attract close fish a bit
        for (int i = 0; i < ObjectSpawner.instance.spawnedObjects.Count; i++)
        {
            var fish = ObjectSpawner.instance.spawnedObjects[i];
            float dist = Vector3.Distance(this.transform.position, fish.transform.position);
            if (dist < maxAttractDistance)
            {
                float attractFactor = (maxAttractDistance - dist) / maxAttractDistance;

                Vector3 dir = (this.transform.position - fish.transform.position) * Time.deltaTime * attractionStrength;
                fish.SetAttractor(dir);
            }

        }
    }

    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Colliding with " + other.gameObject.name);
        MovingThing mt = other.gameObject.GetComponentInParent<MovingThing>();
        if (mt != null)
        {
            mt.EnterThreat(this);
        }
    }



}
