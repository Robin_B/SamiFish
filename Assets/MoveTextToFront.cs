﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTextToFront : MonoBehaviour
{

    void Start()
    {
        this.GetComponent<TextMesh>().GetComponent<Renderer>().sortingOrder = 30;
    }

}
