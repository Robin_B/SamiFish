﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{

    public AudioClip[] peaceSongs;
    public AudioClip[] dangerSongs;
    public AudioClip[] skaiSongs;
    public AudioClip ambient;

    public AudioClip[] effects;

    public static AudioController instance;


    public AudioSource backgroundAudio;
    public AudioSource lyricsAudio;

    public float updateStep = 0.1f;
    public int sampleDataLength = 1024;

    private float currentUpdateTime = 0f;
    private float clipLoudness;
    private float[] clipSampleData;
    private float overallPeak = 0.001f;

    private bool skaiTalk = false;

    public TextMesh subtitles;

    private string t1 = "Balance. Equilibrium. Holy water. Sáivo water. since time immemorial. As long as we remember. So long that others have forgotten. They go with the waves. We are under the waves. We are not concerned about the waves if they are just waves. But if the waves are not just waves then we watch out...   we go with the flows, against the tide. from fairway to another. streams to another. in whirlpools, in pits. bending, following, swimming, tails sprinting, lemmas glistening, fins vibrating. going up the river, taking breaks. pausing and continuing. through waterfalls and rapids, resting in quiet waters. returning to spawning areas. sisters, brothers, fathers, mothers, relatives, partners, friends, fellows, folks. in the sea, in rivers, in lakes, in ponds. trouts, graylings, arctic chars, spikes, whitefish, perch, codfish, satires. we swim in peace, firmly. gills fold and unfold lightly.";
    private string t1NS = "(Language: Northern Sámi) Balance. Equilibrium. Holy water. Sáivo water. since time immemorial. As long as we remember. So long that others have forgotten. They go with the waves. We are under the waves. We are not concerned about the waves if they are just waves. But if the waves are not just waves then we watch out...   we go with the flows, against the tide. from fairway to another. streams to another. in whirlpools, in pits. bending, following, swimming, tails sprinting, lemmas glistening, fins vibrating. going up the river, taking breaks. pausing and continuing. through waterfalls and rapids, resting in quiet waters. returning to spawning areas. sisters, brothers, fathers, mothers, relatives, partners, friends, fellows, folks. in the sea, in rivers, in lakes, in ponds. trouts, graylings, arctic chars, spikes, whitefish, perch, codfish, satires. we swim in peace, firmly. gills fold and unfold lightly.";
    private string t1SS = "(Language: Skolt Sámi) Balance. Equilibrium. Holy water. Sáivo water. since time immemorial. As long as we remember. So long that others have forgotten. They go with the waves. We are under the waves. We are not concerned about the waves if they are just waves. But if the waves are not just waves then we watch out...   we go with the flows, against the tide. from fairway to another. streams to another. in whirlpools, in pits. bending, following, swimming, tails sprinting, lemmas glistening, fins vibrating. going up the river, taking breaks. pausing and continuing. through waterfalls and rapids, resting in quiet waters. returning to spawning areas. sisters, brothers, fathers, mothers, relatives, partners, friends, fellows, folks. in the sea, in rivers, in lakes, in ponds. trouts, graylings, arctic chars, spikes, whitefish, perch, codfish, satires. we swim in peace, firmly. gills fold and unfold lightly.";
    private string t1IS = "(Language: Inari Sámi) Balance. Equilibrium. Holy water. Sáivo water. since time immemorial. As long as we remember. So long that others have forgotten. They go with the waves. We are under the waves. We are not concerned about the waves if they are just waves. But if the waves are not just waves then we watch out...   we go with the flows, against the tide. from fairway to another. streams to another. in whirlpools, in pits. bending, following, swimming, tails sprinting, lemmas glistening, fins vibrating. going up the river, taking breaks. pausing and continuing. through waterfalls and rapids, resting in quiet waters. returning to spawning areas. sisters, brothers, fathers, mothers, relatives, partners, friends, fellows, folks. in the sea, in rivers, in lakes, in ponds. trouts, graylings, arctic chars, spikes, whitefish, perch, codfish, satires. we swim in peace, firmly. gills fold and unfold lightly.";
    private string t2 = "(Language: Northern Sámi) what is that... there's a stronger rapid... quite weird... don't go there, don't try, don't attempt, it will swallow you up, suffocates, pollutes, deprives, takes your life, it's life threatening! this is distressing. water is different, heavy to breathe. gills choking, we lose our breath!";
    private string t3 = "(Language: Northern Sámi) what is this then.. i'm anxious to know... i will check it out, let's go check it out! this is scary.. don't don't don't.. it's weird ways out here.. they try to inprison us.. they try to take our right to be.. the right to live.. the right to survive.. the right to be in equilibrium.. they reduce.. they narrow off.. they steer us.. they cut off.. they control… they boss around.. they regulate.. they command.. they suffocate.. they obliterate.. ";
    private string t4 = "(Language: Northern Sámi) I am SKÁIMMADAS, the ancestor of fish, the fish god. You have been careless, steered the fish to harm's way. These are holy waters that are exposed to exploitation. This holy fountain's water needs to be in peace, people need to let it be holy and be respected. Are they really going to capture this water in plastic bottles and sell it all over the world in senseless production chains just because of some short term money? So that the process opens up a road to rich and careless businessmen to come and rape this holy place? This area has been protected by Sámi families, they have maintained peace here so that the spirits and flows stay like they are supposed to be and the water stays pure. Here is no space for a plastic bottle factory that sucks off this holy water in the name of profit. Don't make the Skáimmadas angry, listen instead!";
    private string t5 = "(Language: Northern Sámi) I am SKÁIMMADAS, the ancestor of fish, the fish god. Didn't you notice the danger? Now fish have lost their lives, you have driven the fish to the state of overly destruction. This is the place where the government is trying to command and dominate and resettle all the rights to the fishing waters to itself. This is the place where they have taken the Sámi self determination away, from the waters that the Sámis have been fishing in traditional sustainable ways for hundreds, even thousands of years. At the same time the Sámis have respected the water in the principle of asking first and taking only what is necessary so that the equilibrium is maintained. The Sámis have now established a Moratorium to show that there still are people in the world that communicate with the river, communicate with the water, communicate with the salmon. They have to have the right to self-determination! When that is permitted, also the fish can be in balanced state, we have a connection with those folks. They understand the principle of balance, they know how to fish right! Don't make the Skáimmadas angry, listen instead!";

    private string twin =
            "That's the way to do it! Skáimmadas approves. You have skillfully instructed the fish to stay in right currents and kept the ongoing balance. At the same time you have combined letters and made cool words. Very good!"
        ;


    private string currentSubtitles;
    private bool startedLyrics = false;
    public Transform subtitleCenter;

    public AudioSource yoik;

    bool startPlaying = true;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        skaiTalk = false;
        clipSampleData = new float[sampleDataLength];
        subtitles.GetComponent<Renderer>().sortingOrder = 30;
    }

    float GetCurrentLoudness()
    {
        currentUpdateTime += Time.deltaTime;
        if (currentUpdateTime >= updateStep)
        {
            currentUpdateTime = 0f;
            lyricsAudio.clip.GetData(clipSampleData, lyricsAudio.timeSamples); //I read 1024 samples, which is about 80 ms on a 44khz stereo clip, beginning at the current sample position of the clip.
            clipLoudness = 0f;
            foreach (var sample in clipSampleData)
            {
                clipLoudness += Mathf.Abs(sample);
            }
            clipLoudness /= sampleDataLength; //clipLoudness is what you are looking for
        }
        return clipLoudness;
    }

    public void PlayEffect(int effect, float volume = 1)
    {
        AudioSource.PlayClipAtPoint(effects[effect], this.transform.position, volume);
    }

    public float GetLyricsProgress()
    {
        float progress = Mathf.Clamp01(lyricsAudio.time / lyricsAudio.clip.length);
        if (progress == 0 && !lyricsAudio.isPlaying) return 1;
        return progress;
    }

    public void StartScreen()
    {
        
    }

    void PlayLyrics(AudioClip c, string subtitleText)
    {
        lyricsAudio.Stop();
        lyricsAudio.clip = c;
        lyricsAudio.Play();
        currentSubtitles = subtitleText;
        subtitles.text = currentSubtitles;
        startedLyrics = true;

    }

    public void NextStage(int newStage)
    {
        skaiTalk = false;
        if (newStage == 1)
        {
            // beginning of the game, pick one language
            //PlayLyrics(peaceSongs[0], t1);
            int language = Random.Range(0, 3);
            if (language == 0) PlayLyrics(peaceSongs[2], t1IS);
            else if (language == 1) PlayLyrics(peaceSongs[3], t1SS);
            else PlayLyrics(peaceSongs[1], t1NS);
        }
        if (newStage == 2)
        {
            PlayLyrics(dangerSongs[0], t2);
        }
        if (newStage == 3)
        {
            PlayLyrics(dangerSongs[1], t3);
        }


    }

    void SkaiTalk()
    {
        //Debug.Log("Current Loudness: " + GetCurrentLoudness());
        float loud = GetCurrentLoudness();
        if (loud > overallPeak) overallPeak = loud;
        float angle = 65.0f * loud / overallPeak;
        if (overallPeak > 0.000000000001f) overallPeak *= 0.99f;
        if (angle > 65.0f) angle = 65;
        Skaimmadas.instance.SetJaw(angle);
        if (GetLyricsProgress() > 0.98f)
        {
            // angry over!
            Skaimmadas.instance.MainGame();
        }
    }

    void SubtitleMover()
    {
        float lyricsProgress = GetLyricsProgress();

        float textLength = currentSubtitles.Length;
        var bounds = subtitles.GetComponent<Renderer>().bounds;
        float subtitleWidth = bounds.size.x;
        if (lyricsProgress > 0.999f)
        {
            subtitles.transform.position += Vector3.left * Time.deltaTime * 10;
        }
        else
        {
            subtitles.transform.position = subtitleCenter.position + Vector3.left * (subtitleWidth * lyricsProgress);
            
        }

    }


    void Update()
    {
        if (skaiTalk) SkaiTalk();
        if (startedLyrics)
            SubtitleMover();

        if (startPlaying && startedLyrics)
        {
            yoik.volume *= 0.99f;
            if (yoik.volume < 0.05f)
            {
                yoik.Stop();
                startPlaying = false;
            }
        }
            
    }

    internal void StartWin()
    {
        skaiTalk = true;
        PlayLyrics(skaiSongs[2], twin);
    }

    internal void StartShouting(int angryCount)
    {
        skaiTalk = true;
        if (angryCount == 1)
            PlayLyrics(skaiSongs[0], t4);
        else
            PlayLyrics(skaiSongs[1], t5);
    }
}
